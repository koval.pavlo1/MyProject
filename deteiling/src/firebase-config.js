// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore/lite'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAYGRHMK9wocjQRmnyIKSNIJwbCx6o2Lws",
  authDomain: "detailing-3b05f.firebaseapp.com",
  projectId: "detailing-3b05f",
  storageBucket: "detailing-3b05f.appspot.com",
  messagingSenderId: "1058779850216",
  appId: "1:1058779850216:web:cf30a1147f18a329ce705d"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
export default db
