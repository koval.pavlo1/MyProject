import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ShippingCard from "../views/ShippingCard.vue";
import AboutView from "../views/AboutView.vue";
import DeteilingActions from "../views/DeteilingActions.vue";
const routes = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },
  {
    path: "/card",
    name: "Card",
    component: ShippingCard,
  },
  { path: "/about", component: AboutView, name: "About" },
  {
    path: "/actions",
    component: DeteilingActions,
    name: "Actions"
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
